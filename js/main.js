var ytjurl = "http://avoindata.prh.fi:80/bis/v1?totalResults=false&maxResults=##MAXRESULTS##&name=##" +
"COMPANY##&registeredOffice=##CITY##&businessLine=##BLINE##&companyRegistrationFrom=2000-01-01";

$("#searchfilterform").hide();

$("#searchButton").click(function(e){
  $.mobile.loading( "show", {
            text: "Ladataan...",
            textVisible: true,
            textonly: false,
    });
  var cr = $("#searchResultsCount").val();
  var cn = $("#searchByCompanyName").val();
  var cc = $("#searchByCompanyCity").val();
  var cb = $("#searchByCompanyLine").val();
  $("#searchResults").html("");

  var turl = ytjurl.replace("##MAXRESULTS##",cr);
  turl = turl.replace("##COMPANY##",cn);
  turl = turl.replace("##CITY##",cc);
  turl = turl.replace("##BLINE##",cb);

  $.get(turl,function(data,status){
    var res = data.results;
    if(res.length > 0) {
      $("#searchfilterform").show();
    }

    for(var k=0;k < res.length;k++){
      // Tulostus
      var op =  '<div bid="'+  res[k].detailsUri +'" class="collbid"  data-role="collapsible" data-filtertext="' + res[k].name + '">' +
                 '<h4>' + res[k].name + " " +  res[k].businessId + '</h4>' +
                '<p class="reginfo">' +  '</p>' +
                '<p class="bforms">' +  '</p>' +
                '<h5>Yhteystiedot</h5>' +
                '<p class="address">' +  '</p>' +
                '<p class="contact1">' +  '</p>' +
            '</div>';
      $("#searchResults").append(op);
    }
    $( "#searchResults" ).collapsibleset( "refresh" );
    $.mobile.loading( "hide" );
    $( ".collbid" ).collapsible({
      expand: getInfo
    });
  });
});

function getInfo(event,ui){
  $.mobile.loading( "show", {
            text: "Ladataan...",
            textVisible: true,
            textonly: false,
    });
  var u = $(this).attr('bid');
  var obj =this;
  $.get(u,function(data2,status2){
    var ris = data2.results[0];
    //console.log(ris);

    $(".reginfo",obj).html("Rekisteröity " + ris.registrationDate );

    var cad = ris.addresses[0];
    $(".address",obj).html(cad.street + " " + cad.postCode + " " + cad.city );

    var contacts = ris.contactDetails;
    $(".contact1",obj).html("");
    for(var b=0;b < contacts.length;b++){
      if(contacts[b].language != "FI") continue;

      if(contacts[b].type=="Kotisivun www-osoite"){
        contacts[b].value = '<a href="http://' + contacts[b].value + '">' + contacts[b].value + '</a>';
      }

      $(".contact1",obj).append(contacts[b].type + " " + contacts[b].value + " " );
    }

    var bforms = ris.businessLines;
    $(".bforms",obj).html("");
    for(var c=0;c < bforms.length;c++){
      if(bforms[c].language != "FI") continue;
      $(".bforms",obj).append(bforms[c].code + "/" + bforms[c].name + " " );
    }

    $.mobile.loading( "hide" );
  });
}
